<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
});

// // Disable Updraft and Wordfence on localhost
// add_action( 'init', function () {
//     if ( !is_admin() && ( strpos( get_site_url(), 'localhost' ) > -1 ) ) {
//         include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

//         $updraft = WP_PLUGIN_DIR . '/updraftplus/updraftplus.php';
//         if ( file_exists( $updraft ) ) deactivate_plugins( [ $updraft ] );

//         $wordfence = WP_PLUGIN_DIR . '/wordfence/wordfence.php';
//         if ( file_exists( $wordfence ) ) deactivate_plugins( [ $wordfence ] );
//     }
//     register_shortcodes();
// } );

// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );


//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );

// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');


function register_shortcodes(){
  $dir=dirname(__FILE__)."/shortcodes";
  $files=scandir($dir);
  foreach($files as $k=>$v){
    $file=$dir."/".$v;
    if(strstr($file,".php")){
      $shortcode=substr($v,0,-4);
      add_shortcode($shortcode,function($attr,$content,$tag){
        ob_start();
        include(dirname(__FILE__)."/shortcodes/".$tag.".php");
        $c=ob_get_clean();
        return $c;
      });
    }
  }
}



// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');





function fr_img($id=0,$size="",$url=false,$attr=""){

    //Show a theme image
    if(!is_numeric($id) && is_string($id)){
        $img=get_stylesheet_directory_uri()."/images/".$id;
        if(file_exists(to_path($img))){
            if($url){
                return $img;
            }
            return '<img src="'.$img.'" '.($attr?build_attr($attr):"").'>';
        }
    }

    //If ID is empty get the current post attachment id
    if(!$id){
        $id=get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if(is_object($id)){
        if(!empty($id->ID)){
            $id=$id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if(get_post_type($id)!="attachment"){
        $id=get_post_thumbnail_id($id);
    }

    if($id){
        $image_url=wp_get_attachment_image_url($id,$size);
        if(!$url){
            //If image is a SVG embed the contents so we can change the color dinamically
            if(substr($image_url,-4,4)==".svg"){
                $image_url=str_replace(get_bloginfo("url"),ABSPATH."/",$image_url);
                $data=file_get_contents($image_url);
                echo strstr($data,"<svg ");
            }else{
                return wp_get_attachment_image($id,$size,0,$attr);
            }
        }else if($url){
            return $image_url;
        }
    }
}

if (@$_GET['keyword'] != '' && @$_GET['brand'] !="") {
    $url = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    $url = explode('?', $url);
    setcookie('keyword', $_GET['keyword']);
    setcookie('brand', $_GET['brand']);
    wp_redirect($url[0]);
    exit;
} elseif (@$_GET['brand'] !="" && @$_GET['keyword'] == '') {
    $url = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    $url = explode('?', $url);
    setcookie('brand', $_GET['brand']);
    wp_redirect($url[0]);
    exit;
} elseif (@$_GET['brand'] =="" && @$_GET['keyword'] != '') {
    $url = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    $url = explode('?', $url);
    setcookie('keyword', $_GET['keyword']);
    wp_redirect($url[0]);
    exit;
}
 
 
// shortcode to show H1 google keyword fields
function new_google_keyword()
{
    if (@$_COOKIE['keyword'] ==""  && @$_COOKIE['brand'] == "") {
        // return $google_keyword = '<h1 class="googlekeyword">Save up to $500 on Flooring<h1>';
        return $google_keyword = '<h1 class="googlekeyword">Save up to $1000 on Flooring<h1>';
    } else {
        $keyword = $_COOKIE['keyword'];
        $brand = $_COOKIE['brand'];
        return $google_keyword = '<h1 class="googlekeyword">Save up to $1000 on '.$brand.' '.$keyword.'<h1>';
    }
}
add_shortcode('google_keyword_code', 'new_google_keyword');

function featured_products_fun() {
	$args = array(
        'post_type' => array('carpeting', 'luxury_vinyl_tile', 'solid_wpc_waterproof', 'hardwood_catalog', 'laminate_catalog', 'tile_catalog'),
        'post__not_in' => array(304916,366219,302354),
        'posts_per_page' => 8,
        'orderby' => 'rand',
        'meta_query' => array(
            array(
                'key' => 'featured', 
                'value' => true, 
                'compare' => '=='
            )
        ) 
    );
    $query = new WP_Query( $args );
    if( $query->have_posts() ){
        $outpout = '<div class="featured-products"><div class="featured-product-list">';
        while ($query->have_posts()) : $query->the_post(); 
            $gallery_images = get_field('gallery_room_images');
            $gallery_img = explode("|",$gallery_images);
            $count = 0;
            // loop through the rows of data
            foreach($gallery_img as  $key=>$value) {
                $room_image = $value;
                if(!$room_image){
                    $room_image = "http://placehold.it/245x310?text=No+Image";
                }
                else{
                    if(strpos($room_image , 's7.shawimg.com') !== false){
                        if(strpos($room_image , 'http') === false){ 
                            $room_image = "http://" . $room_image;
                        }
                        $room_image = $room_image ;
                    } else{
                        if(strpos($room_image , 'http') === false){ 
                            $room_image = "https://" . $room_image;
                        }
                        $room_image= "https://mobilem.liquifire.com/mobilem?source=url[".$room_image . "]&scale=size[524x636]&sink";
                    }
                }
                if($count>0){
                    break;
                }
                $count++;
            }

            $color_name = get_field('color');
            $post_type = get_post_type_object(get_post_type( get_the_ID() ));
            
            $outpout .= '<div class="featured-product-item">
                <div class="featured-inner">
                    <div class="prod-img-wrap">
                        <img src="'.$room_image.'" alt="'.get_the_title().'" />
                        <div class="button-wrapper">
                            <div class="button-wrapper-inner">
                                <a href="'.get_the_permalink().'" class="button alt">View Product</a>
                                <a href="/flooring-coupon/" class="button">Get Coupon</a>
                            </div>
                        </div>
                    </div>
                    <div class="product-info">
                        <h5><a href="'.get_the_permalink().'">'.$color_name.'</a></h5>
                        <h6>'.$post_type->labels->singular_name.'</h6>
                    </div>
                </div>
            </div>';

        endwhile;
        $outpout .= '</div></div>';
        wp_reset_query();
    }  


    return $outpout;
}
add_shortcode( 'featured_products', 'featured_products_fun' );


function customcustomcustomcheck_404($url) {
    $headers=get_headers($url, 1);
   if ($headers[0]!='HTTP/1.1 200 OK') {return true; }else{ return false;}
 }


wp_clear_scheduled_hook( '404_redirection_log_cronjob' );
wp_clear_scheduled_hook( '404_redirection_301_log_cronjob' );
if (!wp_next_scheduled('custom_404_redirection_301_log_cronjob')) {   
   
    wp_schedule_event( time() +  17800, 'daily', 'custom_404_redirection_301_log_cronjob');
}

add_action( 'custom_404_redirection_301_log_cronjob', 'themecustom_404_redirect_hook' ); 


 // custom 301 redirects from  404 logs table
 function themecustom_404_redirect_hook(){
    global $wpdb;    
    write_log('themecustom_404_redirect_hook in function');

    $table_redirect = $wpdb->prefix.'redirection_items';
    $table_name = $wpdb->prefix . "redirection_404";
    $table_group = $wpdb->prefix.'redirection_groups';

     $data_404 = $wpdb->get_results( "SELECT * FROM $table_name" );
     $datum = $wpdb->get_results("SELECT * FROM $table_group WHERE name = 'Products'");
     $redirect_group =  $datum[0]->id;  

    if ($data_404)
    {      
      foreach ($data_404 as $row_404) 
       {            

        if (strpos($row_404->url,'carpet') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {
            write_log($row_404->url);      
            
           $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = customcustomcustomcheck_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            $source_url = $row_404->url;
            $destination_url = '/flooring-products/carpeting/carpeting-catalog/';
            $match_url = rtrim($source_url, '/');

            $data = array("url" => $row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            write_log( 'carpet 301 added ');
         }else{

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');


         }

        }
        else if (strpos($row_404->url,'hardwood') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url);

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');
            $url_404 = home_url().''.$row_404->url;
            $headers_res = customcustomcustomcheck_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            $source_url = $row_404->url;
            $destination_url = '/flooring-products/hardwood-flooring/hardwood-catalog/';
            $match_url = rtrim($source_url, '/');

            $data = array("url" => $row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

           }else{

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

           }

            write_log( 'hardwood 301 added ');

        }
        else if (strpos($row_404->url,'laminate') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url); 

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = customcustomcustomcheck_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            $source_url = $row_404->url;
            $destination_url = '/flooring-products/laminate-flooring/laminate-catalog/';
            $match_url = rtrim($source_url, '/');

            $data = array("url" => $row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }

            write_log( 'laminate 301 added ');


        }
        else if ((strpos($row_404->url,'luxury-vinyl') !== false || strpos($row_404->url,'vinyl') !== false) && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url);  

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = customcustomcustomcheck_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            $source_url = $row_404->url;
            if (strpos($row_404->url,'luxury-vinyl') !== false ){
                $destination_url = '/flooring-products/luxury-vinyl-tile/vinyl-catalog/';
            }elseif(strpos($row_404->url,'vinyl') !== false){
                $destination_url = '/flooring-products/luxury-vinyl-tile/vinyl-catalog/';
            }
            
            $match_url = rtrim($source_url, '/');

            $data = array("url" => $row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');
            }

            write_log( 'luxury-vinyl 301 added ');
        }
        else if (strpos($row_404->url,'tile') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url); 

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = customcustomcustomcheck_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            $destination_url = '/flooring-products/tile/tile-catalog/';
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" => $row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }

            write_log( 'customtile 301 added ');
        }

       }  
    }

 }


  //Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter( 'wpseo_breadcrumb_links', 'wpse_override_yoast_breadcrumb_trail',90 );

function wpse_override_yoast_breadcrumb_trail( $links ) {

    if (is_singular( 'carpeting' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/carpeting/',
            'text' => 'Carpet',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/carpeting/carpeting-catalog/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }
	else  if (is_singular( 'hardwood_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/hardwood-flooring/',
            'text' => 'Hardwood',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/hardwood-flooring/hardwood-catalog/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }
	else  if (is_singular( 'laminate_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/laminate-flooring/',
            'text' => 'Laminate',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/laminate-flooring/laminate-catalog/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }
	else  if (is_singular( 'tile_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/tile/',
            'text' => 'Tile',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/tile/tile-catalog/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }
	else  if (is_singular( 'luxury_vinyl_tile' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/luxury-vinyl-tile/',
            'text' => 'Luxury Vinyl',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring-products/luxury-vinyl-tile/vinyl-catalog/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }
	
    
    return $links;
}

//add method to register event to WordPress init

add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
 

function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
   // write_log($sql_delete);	
}
 add_filter( 'auto_update_plugin', '__return_false' );


 
 wp_clear_scheduled_hook( '301_task_hook_dropped' );
if (!wp_next_scheduled('direct_301_task_hook_dropped')) {
    
         $interval =  getIntervalTime('friday');    
         wp_schedule_event( time() + $interval, 'each_saturday', 'direct_301_task_hook_dropped');
   }
   
  add_action( 'direct_301_task_hook_dropped', 'direct_custom_301_redirects' ); 
 
 // custom 301 redirects from sfn dropped api
 function direct_custom_301_redirects(){
    global $wpdb;
    $table_redirect = $wpdb->prefix.'redirection_items';
    $table = $wpdb->prefix.'redirection_groups';
    $table_posts = $wpdb->prefix.'posts';
	$table_meta = $wpdb->prefix.'postmeta';	

    write_log('In 301 function cron');
   
    
    $datum = $wpdb->get_results("SELECT * FROM $table WHERE name = 'Products'");
    $redirect_group =  $datum[0]->id;  

     $upload = wp_upload_dir();
     $upload_dir = $upload['basedir'];
     $upload_dir = $upload_dir . '/sfn-data';
     $permfile = $upload_dir.'/deleted_product.json';

     // Gives us access to the download_url() and wp_handle_sideload() functions
     require_once( ABSPATH . 'wp-admin/includes/file.php' );
  
     $res ='https://sfn.mm-api.agency/dropped/'.get_option('SITE_CODE');
     
     $tmpfile = download_url( $res, $timeout = 900 );

     $redirect_obj = array();

     $brandmapping = array(
        "/flooring-products/carpeting/carpeting-catalog/"=>"carpeting",
        "/flooring-products/hardwood-flooring/hardwood-catalog/"=>"hardwood_catalog",
        "/flooring-products/laminate-flooring/laminate-catalog/"=>"laminate_catalog",
        "/flooring-products/luxury-vinyl-tile/vinyl-catalog/"=>"luxury_vinyl_tile",
        "/flooring-products/tile/tile-catalog/"=>"tile_catalog"
    );

    $categorymapping = array(
        "carpeting"=>"carpet",
        "hardwood_catalog"=>"hardwood",
        "laminate_catalog"=>"laminate",
        "luxury_vinyl_tile"=>"lvt",
        "tile_catalog"=>"tile"       
    );
  
     if(is_file($tmpfile)){
  
      copy( $tmpfile, $permfile );
      unlink( $tmpfile ); 
    
  
      }
  
      $strJsonFileContents = file_get_contents($permfile);
  
      $handle = json_decode(($strJsonFileContents), true);     
  
        $d = 0;
          foreach($handle as $field => $value){ 

            if($value['category'] != 'rugs'){

           // sleep(1);
             
            write_log('sku----------------'.$value['sku']);

            $find_sku = $value['sku'];

            $pro_category = array_search($value['category'],$categorymapping);
          
            $sql_sku = "SELECT $table_meta.post_id 
						FROM $table_meta
						WHERE  $table_meta.meta_key = 'sku' 
						AND $table_meta.meta_value = '$find_sku'" ;	

               //         write_log($sql_sku);
         
            $query = $wpdb->get_results($sql_sku,ARRAY_A);		

            write_log('<------'.$d.'--------->');

            if (count($query) > 0){
                   
            write_log('Found Post in database>');

            }else{

                write_log('NO Post found');
            }

           // exit;
            
        if (count($query) > 0){

  
                  $post_id = $query[0]['post_id'];

                  write_log('posted id ->'.$post_id );   
                  write_log('SKU ->'.$value['sku'] );  
                  write_log('SKU ->'.$value['category'] );

                  $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$value['sku'].'%"');

                        if($checkalready == ''){ 
                                    
                            $source_url = wp_make_link_relative(get_permalink($post_id));
                            $match_url = rtrim($source_url, '/');

                            write_log($source_url);

                            $post_type =  get_post_type($post_id );

                            $destination_url = array_search($post_type,$brandmapping);

                            write_log($destination_url);

                            $data = array("url" => $source_url,
                                    "match_url" => $match_url,
                                    "match_data" => "",
                                    "action_code" => "301",
                                    "action_type" => "url",
                                    "action_data" => $destination_url,
                                    "match_type" => "url",
                                    "title" => $value['sku'],
                                    "regex" => "true",
                                    "group_id" => $redirect_group,
                                    "position" => "1",
                                    "last_access" => current_time( 'mysql' ),
                                    "status" => "enabled");

                                    $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

                                    $wpdb->insert($table_redirect,$data,$format);                                   
                                   

                                }        

                                if($post_id !=''){

                                    wp_delete_post($post_id);
                    
                                    write_log('Post Deleted form Database as per Dropped API');
                    
        }
     }
  
          

    }
    $d++;
}

     
  write_log('301 custom redirected updated from API is Ended');
  
  }



function storelocation_Fwphone_tel($arg)
{
    
    $website = json_decode(get_option('website_json'));
   
    for ($i=0;$i < count($website->locations);$i++) {
        $location_name = isset($website->locations[$i]->name) ? $website->locations[$i]->name:"";

        $location_ids = isset($website->locations[$i]->id) ? $website->locations[$i]->id:"";
       
        if($website->locations[$i]->type == 'store' &&  $website->locations[$i]->name !=''){

            
            if (in_array(trim($location_name), $arg) || in_array(trim($location_ids), $arg)){

                if($website->locations[$i]->name == 'Main' || $website->locations[$i]->name=="MAIN"){

                    $location_name = get_bloginfo( 'name' );
                    
                }else{
                  
                    $location_name =  isset($website->locations[$i]->name) ? $website->locations[$i]->name." ":"";

                }
      

                $location_phone  = "";
                if (isset($website->locations[$i]->phone)) {
                    $location_tel = preg_replace('/[^0-9]/', '', $website->locations[$i]->phone);
                    $location_phone  = formatPhoneNumber($website->locations[$i]->phone);
                }
                if (isset($website->locations[$i]->forwardingPhone) && $website->locations[$i]->forwardingPhone != "") {
                    $forwarding_tel = preg_replace('/[^0-9]/', '', $website->locations[$i]->forwardingPhone);
                    $forwarding_phone  = formatPhoneNumber($website->locations[$i]->forwardingPhone);
                    
                }else if (isset($website->locations[$i]->phone) && $website->locations[$i]->forwardingPhone = "") {

                    $location_tel = preg_replace('/[^0-9]/', '', $website->locations[$i]->phone);
                    $location_phone  = formatPhoneNumber($website->locations[$i]->phone);
                }
                else{
        
                    $forwarding_tel ="#";
                    $forwarding_phone  = formatPhoneNumber(8888888888);
                }

                if (in_array("forwardingphoneTel", $arg)) {
                    $locations = $forwarding_tel;    
                }
                if (in_array("forwardingphoneTxt", $arg)) {

                    $locations = $forwarding_phone;     
                }
                
            } 

        }        
    }

    return $locations;
}
add_shortcode('storelocation_Fwphone_tel', 'storelocation_Fwphone_tel');

if (! wp_next_scheduled ( 'specialsync_tile_friday_event')) {

    $interval =  getIntervalTime('friday');    
    wp_schedule_event( time(), 'each_friday', 'specialsync_tile_friday_event');
}

add_action( 'specialsync_tile_friday_event', 'special_this_friday_tile', 10, 2 );

function special_this_friday_tile() {

    $product_json =  json_decode(get_option('product_json'));     
    $tile_array = getArrayFiltered('productType','tile',$product_json);
    global $wpdb;
    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/sfn-data';  

    $table_posts = $wpdb->prefix.'posts';
	$table_meta = $wpdb->prefix.'postmeta';	

    // $sql_delete = "DELETE FROM $table_meta WHERE meta_key = 'endpoint' " ;	
    
    
	// $delete_endpoint = $wpdb->get_results($sql_delete);
    
    
   foreach ($tile_array as $tile){

   if( $tile->manufacturer == "Emser"){

    $permfile = $upload_dir.'/tile_catalog_'.$tile->manufacturer.'.json';
    $res = SOURCEURL.get_option('SITE_CODE').'/www/tile/'.$tile->manufacturer.'.json?'.SFN_STATUS_PARAMETER;
   
    $tmpfile = download_url( $res, $timeout = 900 );

    if(is_file($tmpfile)){
        copy( $tmpfile, $permfile );
        unlink( $tmpfile ); 
    }   
   

    write_log('auto_sync - sync_carpet_friday_event-'.$tile->manufacturer);
    $obj = new Example_Background_Processing();
    $obj->handle_all('tile_catalog', $tile->manufacturer);
   
    write_log('Sync Completed - '.$tile->manufacturer);    
}
    }         

    write_log('Sync Completed for all tile brand');   

   // compare_csv_onsite_products('tile_catalog');
    
}
